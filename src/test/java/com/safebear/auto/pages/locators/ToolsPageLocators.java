package com.safebear.auto.pages.locators;

import lombok.Data;
import lombok.Getter;
import org.openqa.selenium.By;

@Data
public class ToolsPageLocators {

    private By successfulLoginMessage = By.xpath(".//p[2]/b");
}
