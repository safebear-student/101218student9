package com.safebear.auto.utils;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.GeckoDriverInfo;

public class Utils {
    private static final String URL = System.getProperty("url","http://toolslist.safebear.co.uk:8080/");
    private static final String BROWSER = System.getProperty("browser","chrome");


    public static String getUrl(){
        return URL;
    }

    public static WebDriver getDriver(){
        System.setProperty("webdriver.chrome.driver","src/test/resources/drivers/chromedriver.exe");
        System.setProperty("webdriver.gecko.driver","src\\test\\resources\\drivers\\geckodriver.exe");

        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.addArguments("window-size=1366,768");

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("window-size=1366,768");



        switch(BROWSER){
            case "chrome":
                return new ChromeDriver(chromeOptions);

            case "tabletsizedchrome":
                ChromeOptions smalloptions = new ChromeOptions();
                smalloptions.addArguments("window-size=600,300");
                return new ChromeDriver(smalloptions);

            case "firefox":
                return new FirefoxDriver(firefoxOptions);

            case "chromeheadless":
//                ChromeOptions headlessOptions = new ChromeOptions();
                chromeOptions.addArguments("headless","disable-gpu");
                return new ChromeDriver(chromeOptions);

            case "edge":
                return new EdgeDriver();


            default:
                return new ChromeDriver(chromeOptions);
        }


    }
}


