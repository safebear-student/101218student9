package com.safebear.auto.tests;
import com.safebear.auto.utils.Utils;
import cucumber.api.CucumberOptions;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.apache.catalina.filters.WebdavFixFilter;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;


@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber"},
        tags = {"~@to-do"},
        glue = "com.safebear.auto.tests",
        features = "classpath:toolslist.features/login.feature"
)


public class RunCukes extends AbstractTestNGCucumberTests {

  }

